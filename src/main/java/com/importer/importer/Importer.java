package com.importer.importer;

import com.importer.user.User;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Importer {

    public List<User> readExcel() {
        try (Stream<String> lines = Files.lines(Paths.get("src/main/java/com/importer/importer/test_case.csv").toAbsolutePath())) {
            return lines.skip(1).map(mapToUsers).collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    private Function<String, User> mapToUsers = (line) -> {
        String[] lines = line.split(";");

        User item = new User();

        item.setId(lines[0].trim());
        item.setTime(Integer.parseInt(lines[1].trim()));
        item.setGroup(lines[2].trim());
        item.setType(lines[3].trim());
        item.setSubType(lines[4].trim());
        item.setUrl(lines[5].trim());
        item.setOrgId(lines[6].trim());
        item.setFormId(lines[7].trim());
        item.setItpa(lines[9].trim());
        item.setSubirresponse(lines[10].trim());

        DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_TIME;
        String s = lines[11].trim();
        if (s.length()>10) {
            String substring = s.substring(0, 10);
            final LocalDateTime time = LocalDateTime.parse(substring + "T" + s.substring(11) + ":00:00");
            item.setDate(time);
        }

        return item;
    };


}


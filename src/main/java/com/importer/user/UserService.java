package com.importer.user;

import com.importer.utils.ListComparator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserService {
    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void saveAll(List<User> users) {
        userRepository.saveAllUsers(users);
    }

    public List<User> getAll() {
        return userRepository.getAllUsers();
    }

    public List<String> getFiveForms() {
        List<String> allForms = userRepository.getAllForms();
        Map<Integer, List<String>> collect = allForms.stream().collect(Collectors.groupingBy(String::hashCode));
        ArrayList<List<String>> lists = new ArrayList<>(collect.values());
        lists.sort(new ListComparator());
        Collections.reverse(lists);
        List<String> fiveForms = new ArrayList<>();
        for (int i = 0; i<5; i++) {
            fiveForms.add(lists.get(i).get(0));
        }
        return fiveForms;
    }
}

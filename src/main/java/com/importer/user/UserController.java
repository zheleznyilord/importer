package com.importer.user;

import com.importer.importer.Importer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "save")
    public String save(){
        Importer importer = new Importer();

        List<User> users = importer.readExcel();

        userService.saveAll(users);

        return "hello";
    }

    @GetMapping(value = "users")
    public String users(Model model){
        List<User> users = userService.getAll();
        Map<String, List<User>> collect = users.stream().collect(Collectors.groupingBy(User::getId));
        model.addAttribute("users",collect);
        return "users";
    }

    @GetMapping(value = "forms")
    public String forms(Model model){
        List<String> fiveForms = userService.getFiveForms();
        model.addAttribute("forms",fiveForms);
        return "forms";
    }

}

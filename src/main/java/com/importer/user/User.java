package com.importer.user;

import java.time.LocalDateTime;

public class User {
    private String id;
    private int time;
    private String group;
    private String type;
    private String subType;
    private String url;
    private String orgId;
    private String formId;
    private String itpa;
    private String subirresponse;
    private LocalDateTime date;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getFormId() {
        return formId;
    }

    public void setFormId(String formId) {
        this.formId = formId;
    }

    public String getItpa() {
        return itpa;
    }

    public void setItpa(String itpa) {
        this.itpa = itpa;
    }

    public String getSubirresponse() {
        return subirresponse;
    }

    public void setSubirresponse(String subirresponse) {
        this.subirresponse = subirresponse;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (time != user.time) return false;
        if (id != null ? !id.equals(user.id) : user.id != null) return false;
        if (group != null ? !group.equals(user.group) : user.group != null) return false;
        if (type != null ? !type.equals(user.type) : user.type != null) return false;
        if (subType != null ? !subType.equals(user.subType) : user.subType != null) return false;
        if (url != null ? !url.equals(user.url) : user.url != null) return false;
        if (orgId != null ? !orgId.equals(user.orgId) : user.orgId != null) return false;
        if (formId != null ? !formId.equals(user.formId) : user.formId != null) return false;
        if (itpa != null ? !itpa.equals(user.itpa) : user.itpa != null) return false;
        if (subirresponse != null ? !subirresponse.equals(user.subirresponse) : user.subirresponse != null)
            return false;
        return date != null ? date.equals(user.date) : user.date == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + time;
        result = 31 * result + (group != null ? group.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (subType != null ? subType.hashCode() : 0);
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (orgId != null ? orgId.hashCode() : 0);
        result = 31 * result + (formId != null ? formId.hashCode() : 0);
        result = 31 * result + (itpa != null ? itpa.hashCode() : 0);
        result = 31 * result + (subirresponse != null ? subirresponse.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", time=" + time +
                ", group='" + group + '\'' +
                ", type='" + type + '\'' +
                ", subType='" + subType + '\'' +
                ", url='" + url + '\'' +
                ", orgId='" + orgId + '\'' +
                ", formId='" + formId + '\'' +
                ", itpa='" + itpa + '\'' +
                ", subirresponse='" + subirresponse + '\'' +
                ", date=" + date +
                '}';
    }
}

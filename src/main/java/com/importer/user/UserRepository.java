package com.importer.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Repository
public class UserRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public UserRepository(NamedParameterJdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }


    public int[] saveAllUsers(List<User> users) {
        List<MapSqlParameterSource> batchArgs = new ArrayList<>();

        updateBatchArgs(users, batchArgs);

        String sql = "INSERT INTO users(id, time, groupp, type, subtype, url, orgid, formid, itpa, subirresponse, date) " +
                "VALUES (:id, :time, :groupp, :type, :subType, :url, :orgId, :formId, :itpa, :subirresponse, :date)";

        return jdbcTemplate.batchUpdate(sql, batchArgs.toArray(new MapSqlParameterSource[users.size()]));
    }

    private void updateBatchArgs(List<User> users, List<MapSqlParameterSource> batchArgs) {
        for (User user : users) {
            MapSqlParameterSource parameterSource = new MapSqlParameterSource();

            parameterSource.addValue("id", user.getId());
            parameterSource.addValue("time", user.getTime());
            parameterSource.addValue("groupp", user.getGroup());
            parameterSource.addValue("type", user.getType());
            parameterSource.addValue("subType", user.getSubType());
            parameterSource.addValue("url", user.getUrl());
            parameterSource.addValue("orgId", user.getOrgId());
            parameterSource.addValue("formId", user.getFormId());
            parameterSource.addValue("itpa", user.getItpa());
            parameterSource.addValue("subirresponse", user.getSubirresponse());
            parameterSource.addValue("date", user.getDate());

            batchArgs.add(parameterSource);
        }
    }

    private static RowMapper<User> userMapper = (rs, rowNum) -> {
        User user = new User();

        user.setId(rs.getString("id"));
        user.setTime(rs.getInt("time"));
        user.setGroup(rs.getString("groupp"));
        user.setType(rs.getString("type"));
        user.setSubType(rs.getString("subType"));
        user.setUrl(rs.getString("url"));
        user.setOrgId(rs.getString("orgId"));
        user.setFormId(rs.getString("formId"));
        user.setItpa(rs.getString("itpa"));
        user.setSubirresponse(rs.getString("subirresponse"));
        Timestamp date = rs.getTimestamp("date");
        if (Objects.nonNull(date)){
            user.setDate(date.toLocalDateTime());
        }

        return user;
    };


    public List<User> getAllUsers(){
        return jdbcTemplate.query("SELECT * FROM users", userMapper);
    }

    public List<String> getAllForms(){
        return jdbcTemplate.queryForList("SELECT formid FROM users", new MapSqlParameterSource(),String.class);
    }
}

package com.importer.utils;

import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class ListComparator implements Comparator<List<String>> {
    @Override
    public int compare(List<String> o1, List<String> o2) {
        return o1.size() - o2.size();
    }
}

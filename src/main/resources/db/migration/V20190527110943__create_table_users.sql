CREATE TABLE IF NOT EXISTS users(
      openId SERIAL PRIMARY KEY,
      id VARCHAR,
      time INTEGER,
      groupp VARCHAR,
      type VARCHAR,
      subType VARCHAR,
      url VARCHAR,
      orgId VARCHAR,
      formId VARCHAR,
      itpa VARCHAR,
      subirresponse VARCHAR,
      date TIMESTAMP
      )
